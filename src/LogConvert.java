import javax.swing.*;
import javax.swing.table.DefaultTableModel;     // Import wykonany na rzecz obsługi tabeli - logTabel
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;      // Import wykonany na rzecz konieczności zmiany formatu czasu


/** Klasa LogConvert zawiera część kodu do wywołania w aplkikacji możliwości zapisu wykonanych konwersji.
 *  Odpowiana ona za utworzenie pliku i nadzur nad nim oraz przeprowadzane operacje.
 **/
public class LogConvert
{
    /** Utworzenie pliku o nazwie LogKonwerter na którym będą przeprowadzane żądzne operacje */
    private File logFile = new File("LogKonwerter.txt");
    private Language language = new Language();
    private int chooseLanguage;


    public LogConvert() {

    }


    public void setChooseLanguage (int appLanguage) {
        chooseLanguage = appLanguage;
    }


    /** Metoda odpowiezialna za zczytane z pliku LogKonwerter danych i zapisanie do tabeli */
    public void readLogFromFile(JTable logTable) throws IOException
    {
        if (!logFile.exists())
        {
            String logFileError = language.getLanguageVersion(chooseLanguage, 21);
            JOptionPane.showMessageDialog(null, logFileError);

            // Wprzypadku gdy plik nie istnieje zostaje utworzony nowy plik o tej samej nazwie
            do
                {
                    logFile.createNewFile();
                }
            while(!logFile.exists());
        }

        String line;
        int indexLine = 0;

        String[] logValues = new String[3];
        BufferedReader read = new BufferedReader(new FileReader("LogKonwerter.txt"));

        DefaultTableModel model = (DefaultTableModel) logTable.getModel();
        while ((line = read.readLine()) != null)
        {
            logValues[indexLine] = line;
            indexLine++;

            if (indexLine == 3)
            {
                // Dodanie wiersza z zczytanymi danymi do taleli
                model.addRow(new Object[]{logValues[0], logValues[1], logValues[2]});
                indexLine = 0;
            }
        }
        read.close();
    }


    /** Metoda która na rzcz pliku "logKonwerter" wykonuje na nim operacje czyszczenia zapisu */
    public void askClearLog(JTable logTable) throws IOException
    {
        DefaultTableModel model = (DefaultTableModel) logTable.getModel();
        String clearMessage = language.getLanguageVersion(chooseLanguage, 20);

        if(logTable.getRowCount() > 10)
        {
            // Wyświetlenie okna dialogowego z koniecznością potwierdzenia chęci wkonania operacji
            if(JOptionPane.showConfirmDialog(null, clearMessage, "", JOptionPane.YES_NO_OPTION) == 0)
            {
                while (logTable.getRowCount() != 0)
                {
                    model.removeRow(0);
                }
            }
        }
        writeLogToFile(logTable);
    }


    /** Metoda która zczytuje dane z tabeli i zapisuje je do pliku */
    public void writeLogToFile(JTable logTable) throws IOException
    {
        FileOutputStream write = new FileOutputStream(logFile);
        BufferedWriter buferWrite = new BufferedWriter(new OutputStreamWriter(write));

        int lineInLogTable = logTable.getRowCount();
        for (int i = 0; i < lineInLogTable; i++)
        {
            for (int j = 0; j <= 2; j++)
            {
                buferWrite.write(logTable.getModel().getValueAt(i, j).toString());
                buferWrite.newLine();
            }
        }
        buferWrite.close();
    }


    /** Metoda odpowiedzialna za dodanie do tabeli kolejnego wiersza z danymi wykonanej operacji */
    public void setLogToTable(JTable logTable, String valueBefore, String unitBefore, String valueAfter, String unitAfter)
    {
        DefaultTableModel model = (DefaultTableModel) logTable.getModel();

        valueBefore = valueBefore.concat(" ").concat(unitBefore);       // Połączenie dwóch łańcuchów znakowych
        valueAfter = valueAfter.concat(" ").concat(unitAfter);          // Połączenie dwóch łańcuchów znakowych

        //Odczyt i zapis do zmiennej actualDate daty i czsu oraz formatowanie zmiennej actualDate na rządany format
        LocalDateTime actualDate = LocalDateTime.now();
        DateTimeFormatter formatLogDate = DateTimeFormatter.ofPattern("dd-MM-yyyy  HH:mm:ss");
        String logDateConvert = actualDate.format(formatLogDate);

        // Dodanie wiersza do tabeli
        model.insertRow(0, new Object[] {valueBefore, valueAfter, logDateConvert});
    }
}