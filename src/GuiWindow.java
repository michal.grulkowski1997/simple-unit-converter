import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.io.IOException;
import java.text.DecimalFormat;         //Wykorzystanie dla funkcji zamiany liczby o nieokreślinej liczbie cyfr po przecinku na oczekiwaną

import java.util.Locale;


/** Klasa WindowMenu zawiera część kodu do wywołania aplkikacji w wersji okienkowej. Na jej rzecz wywoływane są
 *  inne metody zawarte w klasach Currency, Surface oraz Weight konieczne do przeprowadzenia konwersji
 **/
class GuiWindow {

    /**
     * Pola prywatne zawierające komponenty urzywane w aplikacji
     */
    private JFrame frame;
    private JPanel mainPanel = new JPanel();
    private DefaultListCellRenderer listRenderer = new DefaultListCellRenderer();

    private JLabel textTitle;
    private JLabel textChose;
    private JTextField valueToConvertTextFild;
    private JLabel unitBeforeValue;
    private JLabel textOption;

    private JComboBox languageBox;
    private JComboBox walutaBox;
    private JComboBox powierzchniaBox;
    private JComboBox wagaBox;

    private JLabel resultTextInfo;
    private JTextField resultAfterConvert;
    private JLabel unitResultAfterConvert;

    private LogConvert logConvert = new LogConvert();
    private JTable logTable;

    private JLabel grafic;      //Obrazek w dolnym lewym rogu
    private  boolean suspended = false;


    //----------------------------------------------------------------

    /**
     * Pola prywatne zawierające zmienne urzywane w aplikacji do przypisania tekstu z wybranego języka
     */
    private Language language = new Language();
    private int chooseLanguage;

    private String languageErrorLogFile;
    private String languageErrorValueToConvert;
    private String languageErrorValueToConvertMessage;

    /**
     * Metoda prywatna setLanguageText() odpowiedzialna jest za przypisanie textu w wybranym jezyku działania aplikacji.
     **/

    private void getDefaultLocale(){
        if(Locale.getDefault() == Locale.US | Locale.getDefault() == Locale.US){
            chooseLanguage = 1;
        }
        else{
            chooseLanguage = 0;
        }
    }

    private void setLanguageInAllApp(int chooseLanguage) throws IOException {
        logConvert.setChooseLanguage(chooseLanguage);
        language.setChooseLanguage(chooseLanguage);

        textTitle.setText(language.getLanguageVersion(chooseLanguage, 2));
        textChose.setText(language.getLanguageVersion(chooseLanguage, 3));
        textOption.setText(language.getLanguageVersion(chooseLanguage, 4));

        resultTextInfo.setText(language.getLanguageVersion(chooseLanguage, 14));

        String[] languageLog = new String[3];
        languageLog[0] = language.getLanguageVersion(chooseLanguage, 16);
        languageLog[1] = language.getLanguageVersion(chooseLanguage, 17);
        languageLog[2] = language.getLanguageVersion(chooseLanguage, 18);

        logTable.setModel(new DefaultTableModel(new Object[][]{}, languageLog));
        logConvert.readLogFromFile(logTable);

        languageErrorLogFile = language.getLanguageVersion(chooseLanguage, 19);
        languageErrorValueToConvert = language.getLanguageVersion(chooseLanguage, 15);
        languageErrorValueToConvertMessage = language.getLanguageVersion(chooseLanguage, 23);

        languageBox.removeAllItems();
        languageBox.addItem(language.getLanguageVersion(0, 0));
        languageBox.addItem(language.getLanguageVersion(1, 0));
        languageBox.setSelectedIndex(chooseLanguage);

        walutaBox.removeAllItems();
        walutaBox.addItem(language.getLanguageVersion(chooseLanguage, 5));
        walutaBox.addItem(language.getLanguageVersion(chooseLanguage, 6));
        walutaBox.addItem(language.getLanguageVersion(chooseLanguage, 7));
        walutaBox.setSelectedIndex(0);

        powierzchniaBox.removeAllItems();
        powierzchniaBox.addItem(language.getLanguageVersion(chooseLanguage, 8));
        powierzchniaBox.addItem(language.getLanguageVersion(chooseLanguage, 9));
        powierzchniaBox.addItem(language.getLanguageVersion(chooseLanguage, 10));
        powierzchniaBox.setSelectedIndex(0);

        wagaBox.removeAllItems();
        wagaBox.addItem(language.getLanguageVersion(chooseLanguage, 11));
        wagaBox.addItem(language.getLanguageVersion(chooseLanguage, 12));
        wagaBox.addItem(language.getLanguageVersion(chooseLanguage, 13));
        wagaBox.setSelectedIndex(0);
    }


    /**
     * Metoda prywatna setFrameAndPanel() odpowiedzialna jest za utworzenie ramki aplikacji, umieszczenie na niej
     * głównego panelu na którym umieszczane będą komponenty oraz do określenia wymiarów tej ramki
     **/
    private void setFrameAndPanel() {
        frame = new JFrame("Converter");
        frame.setBounds(200, 200, 1040, 300);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);      //Uniemożliwienie maksymalizacji okna aplikacji

        frame.add(mainPanel);
        mainPanel.setLayout(null);
    }


    /**
     * Metoda setInfoText() odpowiedzialna jest za rozmieszczenie na panelu componentów "biernych" takich jak:
     * Jlabel - Podanie informacji (napisu). Niezmieniana w trakcie działania programu
     **/
    private void setInfoLabel() {
        textTitle = new JLabel("");
        textTitle.setBounds(15, 10, 525, 20);
        textTitle.setFont(new Font("SansSerif", Font.BOLD, 15));
        textTitle.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(textTitle);

        textChose = new JLabel("");
        textChose.setBounds(195, 40, 150, 15);
        textChose.setFont((new Font("SansSerif", Font.PLAIN, 14)));
        textChose.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(textChose);

        unitBeforeValue = new JLabel("");
        unitBeforeValue.setBounds(335, 65, 80, 25);
        mainPanel.add(unitBeforeValue);

        textOption = new JLabel("");
        textOption.setBounds(120, 105, 300, 15);
        textOption.setFont((new Font("SansSerif", Font.PLAIN, 14)));
        textOption.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(textOption);

        resultTextInfo = new JLabel("");
        resultTextInfo.setBounds(195, 180, 150, 15);
        resultTextInfo.setFont((new Font("SansSerif", Font.BOLD, 14)));
        resultTextInfo.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(resultTextInfo);

        unitResultAfterConvert = new JLabel("");
        unitResultAfterConvert.setBounds(340, 210, 30, 25);
        mainPanel.add(unitResultAfterConvert);
    }


    /**
     * Metoda setFildAndBox() odpowiedzialna jest za rozmieszczenie na panelu componentów "aktywnych" takich jak:
     * JTextFild - pole do podawania wartości
     * JComboBox - wybór opcji konwersji
     **/
    private void setFildAndBox() {
        valueToConvertTextFild = new JTextField("");
        valueToConvertTextFild.setBounds(210, 65, 120, 25);
        mainPanel.add(valueToConvertTextFild);
        valueToConvertTextFild.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                changeValueListener();
                suspend();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                changeValueListener();
                suspend();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                changeValueListener();

            }
        });

        languageBox = new JComboBox();
        languageBox.setBounds(480, 50, 50, 25);
        listRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        languageBox.setRenderer(listRenderer);
        mainPanel.add(languageBox);

        walutaBox = new JComboBox();
        walutaBox.setBounds(50, 135, 120, 25);
        listRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        walutaBox.setRenderer(listRenderer);
        mainPanel.add(walutaBox);

        powierzchniaBox = new JComboBox();
        powierzchniaBox.setBounds(195, 135, 150, 25);
        listRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        powierzchniaBox.setRenderer(listRenderer);
        mainPanel.add(powierzchniaBox);

        wagaBox = new JComboBox();
        wagaBox.setBounds(370, 135, 120, 25);
        listRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        wagaBox.setRenderer(listRenderer);
        mainPanel.add(wagaBox);

        resultAfterConvert = new JTextField("");
        resultAfterConvert.setBounds(205, 210, 130, 25);
        resultAfterConvert.setEditable(false);
        resultAfterConvert.setHorizontalAlignment(SwingConstants.CENTER);
        mainPanel.add(resultAfterConvert);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(555, 15, 450, 230);
        mainPanel.add(scrollPane);

        logTable = new JTable();
        logTable.getTableHeader().setResizingAllowed(false);
        logTable.getTableHeader().setReorderingAllowed(false);
        logTable.setEnabled(false);
        scrollPane.setViewportView(logTable);
    }


    /**
     * Metoda setComponents() uruchamia metody prywatne odpowiedzialne na cały wygląd gui aplikacji
     */
    public void setComponents() throws IOException {
        setFrameAndPanel();
        setFildAndBox();
        setInfoLabel();
        frame.setVisible(true);

        getDefaultLocale();
        setLanguageInAllApp(chooseLanguage);

        logConvert.askClearLog(logTable);
    }


    private void changeValueListener() {
        unitBeforeValue.setText("");
        walutaBox.setSelectedIndex(0);
        powierzchniaBox.setSelectedIndex(0);
        wagaBox.setSelectedIndex(0);
        resultAfterConvert.setText("");
        unitResultAfterConvert.setText("");
    }


    private void languageListener() {
        languageBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    chooseLanguage = languageBox.getSelectedIndex();
                    languageBox.setSelectedIndex(chooseLanguage);
                    language.setChooseLanguage(chooseLanguage);
                    logConvert.setChooseLanguage(chooseLanguage);
                    setLanguageInAllApp(chooseLanguage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Metoda prywatna walutaListener odpowiedzialna jest za pobranie wartości do konwersji, przetworzenie
     * zgodnie z oczekiwaną opcją oraz wyświetlenie. Metoda ta wykorzystue metody składową klasy Currency
     * do wykonania rządanej konwersji
     **/
    private void walutaListener() {
        walutaBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                resume(threadIcon);

                double doubleBeforeConvert;
                double doubleAfterConvert;
                String stringAfterConvert;
                String stringBeforeConvert;

                /** Obsługa wyjątku polegająca na wyświetleniu w oknie wynikowym komunikatu "ZŁY FORMAT"
                 *  oraz instrukcją powsępowania w osobnym oknie dialogowym by ten bład naprawić
                 **/
                try {
                    switch (walutaBox.getSelectedIndex()) {
                        case 1:
                            stringBeforeConvert = valueToConvertTextFild.getText();
                            doubleBeforeConvert = Double.parseDouble(stringBeforeConvert);
                            if (doubleBeforeConvert < 0) {
                                doubleBeforeConvert *= -1.0;
                            }
                            doubleAfterConvert = Currency.pound2PLN(doubleBeforeConvert);

                            stringBeforeConvert = (new DecimalFormat("#0.00").format(doubleBeforeConvert));
                            valueToConvertTextFild.setText(String.valueOf(doubleBeforeConvert));

                            stringAfterConvert = (new DecimalFormat("#0.00").format(doubleAfterConvert));
                            resultAfterConvert.setText(stringAfterConvert);
                            unitResultAfterConvert.setText("PLN");
                            unitBeforeValue.setText("GBP");

                            logConvert.setLogToTable(logTable, stringBeforeConvert, "GBP", stringAfterConvert, "PLN");
                            logConvert.writeLogToFile(logTable);

                            powierzchniaBox.setSelectedIndex(0);
                            wagaBox.setSelectedIndex(0);
                            break;

                        case 2:
                            stringBeforeConvert = valueToConvertTextFild.getText();
                            doubleBeforeConvert = Double.parseDouble(stringBeforeConvert);
                            if (doubleBeforeConvert < 0) {
                                doubleBeforeConvert *= -1.0;
                            }
                            doubleAfterConvert = Currency.euro2PLN(doubleBeforeConvert);

                            stringBeforeConvert = (new DecimalFormat("#0.00").format(doubleBeforeConvert));
                            valueToConvertTextFild.setText(String.valueOf(doubleBeforeConvert));

                            stringAfterConvert = (new DecimalFormat("#0.00").format(doubleAfterConvert));
                            resultAfterConvert.setText(stringAfterConvert);
                            unitResultAfterConvert.setText("PLN");
                            unitBeforeValue.setText("EURO");

                            logConvert.setLogToTable(logTable, stringBeforeConvert, "EURO", stringAfterConvert, "PLN");
                            logConvert.writeLogToFile(logTable);

                            powierzchniaBox.setSelectedIndex(0);
                            wagaBox.setSelectedIndex(0);
                            break;
                    }
                } catch (NumberFormatException e) {
                    resume(threadIcon);
                    resultAfterConvert.setText(languageErrorValueToConvert);
                    JOptionPane.showMessageDialog(null, languageErrorValueToConvertMessage);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, languageErrorLogFile);
                }
            }
        });
    }


    /**
     * Metoda prywatna powierzchniaListener odpowiedzialna jest za pobranie wartości do konwersji, przetworzenie
     * zgodnie z oczekiwaną opcją oraz wyświetlenie. Metoda ta wykorzystue metody składową klasy Surface
     * do wykonania rządanej konwersji
     **/
    private void powierzchniaListener() {
        powierzchniaBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                resume(threadIcon);

                double doubleBeforeConvert;
                double doubleAfterConvert;
                String stringAfterConvert;
                String stringBeforeConvert;

                /** Obsługa wyjątku polegająca na wyświetleniu w oknie wynikowym komunikatu "ZŁY FORMAT"
                 *  oraz instrukcją powsępowania w osobnym oknie dialogowym by ten bład naprawić
                 **/
                try {
                    switch (powierzchniaBox.getSelectedIndex()) {
                        case 1:
                            stringBeforeConvert = valueToConvertTextFild.getText();
                            doubleBeforeConvert = Double.parseDouble(stringBeforeConvert);
                            if (doubleBeforeConvert < 0) {
                                doubleBeforeConvert *= -1.0;
                            }
                            doubleAfterConvert = Surface.acres2m_kw(doubleBeforeConvert);

                            stringBeforeConvert = (new DecimalFormat("#0.00").format(doubleBeforeConvert));
                            valueToConvertTextFild.setText(String.valueOf(doubleBeforeConvert));

                            stringAfterConvert = (new DecimalFormat("#0.00").format(doubleAfterConvert));
                            resultAfterConvert.setText(stringAfterConvert);
                            unitResultAfterConvert.setText("M^2");
                            unitBeforeValue.setText("Akr");

                            logConvert.setLogToTable(logTable, stringBeforeConvert, "Akr", stringAfterConvert, "M^2");
                            logConvert.writeLogToFile(logTable);

                            walutaBox.setSelectedIndex(0);
                            wagaBox.setSelectedIndex(0);
                            break;

                        case 2:
                            stringBeforeConvert = valueToConvertTextFild.getText();
                            doubleBeforeConvert = Double.parseDouble(stringBeforeConvert);
                            if (doubleBeforeConvert < 0) {
                                doubleBeforeConvert *= -1.0;
                            }
                            doubleAfterConvert = Surface.sf2m_kw(doubleBeforeConvert);

                            stringBeforeConvert = (new DecimalFormat("#0.00").format(doubleBeforeConvert));
                            valueToConvertTextFild.setText(String.valueOf(doubleBeforeConvert));

                            stringAfterConvert = (new DecimalFormat("#0.00").format(doubleAfterConvert));
                            resultAfterConvert.setText(stringAfterConvert);
                            unitResultAfterConvert.setText("M^2");
                            unitBeforeValue.setText("Ft");

                            logConvert.setLogToTable(logTable, stringBeforeConvert, "Ft", stringAfterConvert, "M^2");
                            logConvert.writeLogToFile(logTable);

                            walutaBox.setSelectedIndex(0);
                            wagaBox.setSelectedIndex(0);
                            break;
                    }
                } catch (NumberFormatException e) {
                    resume(threadIcon);
                    resultAfterConvert.setText(languageErrorValueToConvert);
                    JOptionPane.showMessageDialog(null, languageErrorValueToConvertMessage);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, languageErrorLogFile);
                }
            }
        });

    }


    /**
     * Metoda prywatna wagaListener odpowiedzialna jest za pobranie wartości do konwersji, przetworzenie
     * zgodnie z oczekiwaną opcją oraz wyświetlenie. Metoda ta wykorzystue metody składową klasy Weight
     * do wykonania rządanej konwersji
     **/
    private void wagaListener() {
        wagaBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {

                resume(threadIcon);


                double doubleBeforeConvert;
                double doubleAfterConvert;
                String stringAfterConvert;
                String stringBeforeConvert;

                /** Obsługa wyjątku polegająca na wyświetleniu w oknie wynikowym komunikatu "ZŁY FORMAT"
                 *  oraz instrukcją powsępowania w osobnym oknie dialogowym by ten bład naprawić
                 **/
                try {
                    switch (wagaBox.getSelectedIndex()) {
                        case 1:
                            // Zczytanie z komponentu
                            stringBeforeConvert = valueToConvertTextFild.getText();
                            // Zamiana typu string na double oraz zapis do zmiennej
                            doubleBeforeConvert = Double.parseDouble(stringBeforeConvert);
                            // Obsługa wyjątku liczb ujemnych
                            if (doubleBeforeConvert < 0) {
                                doubleBeforeConvert *= -1.0;
                            }
                            // Konwersja liczby na inną jednostkę oraz zapis do zmiennej
                            doubleAfterConvert = Weight.pound2kg(doubleBeforeConvert);

                            // Formatowanie po zmianie znaku i wstawienie do "valueToConvertTextFild"
                            stringBeforeConvert = (new DecimalFormat("#0.00").format(doubleBeforeConvert));
                            valueToConvertTextFild.setText(String.valueOf(doubleBeforeConvert));

                            // Zamiana typu double na string oraz obcięcie określonej ilości cyfr po przecinku
                            stringAfterConvert = (new DecimalFormat("#0.00").format(doubleAfterConvert));
                            // Wysłanie zmiennej typu strign do komponetu resultAfterConvert
                            resultAfterConvert.setText(stringAfterConvert);
                            //Dodanie jednostki wynikowej
                            unitResultAfterConvert.setText("Kg");
                            unitBeforeValue.setText("Lb");

                            // Wstawienie do tabeli log-ów z przeliczeń
                            logConvert.setLogToTable(logTable, stringBeforeConvert, "Lb", stringAfterConvert, "Kg");
                            // Zapis do pliku log-ów
                            logConvert.writeLogToFile(logTable);

                            // Ustawienie pozostałych komponentów JComboBox w stan wyświetlania opcji
                            powierzchniaBox.setSelectedIndex(0);
                            walutaBox.setSelectedIndex(0);
                            break;

                        case 2:

                            stringBeforeConvert = valueToConvertTextFild.getText();
                            doubleBeforeConvert = Double.parseDouble(stringBeforeConvert);
                            if (doubleBeforeConvert < 0) {
                                doubleBeforeConvert *= -1.0;
                            }
                            doubleAfterConvert = Weight.ounce2kg(doubleBeforeConvert);

                            stringBeforeConvert = (new DecimalFormat("#0.00").format(doubleBeforeConvert));
                            valueToConvertTextFild.setText(String.valueOf(doubleBeforeConvert));

                            stringAfterConvert = (new DecimalFormat("#0.00").format(doubleAfterConvert));
                            resultAfterConvert.setText(stringAfterConvert);
                            unitResultAfterConvert.setText("Kg");
                            unitBeforeValue.setText("Oz");

                            logConvert.setLogToTable(logTable, stringBeforeConvert, "Oz", stringAfterConvert, "Kg");
                            logConvert.writeLogToFile(logTable);

                            powierzchniaBox.setSelectedIndex(0);
                            walutaBox.setSelectedIndex(0);
                            break;
                    }
                } catch (NumberFormatException e) {
                    resume(threadIcon);
                    resultAfterConvert.setText(languageErrorValueToConvert);
                    JOptionPane.showMessageDialog(null, languageErrorValueToConvertMessage);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null, languageErrorLogFile);
                }
            }
        });
    }


    /**
     * Metoda publiczna guiMenu() odpowiedzialna jest za zebranie poszczegónych funkcji składowych w jedną metodę
     * publiczną uruchamiającą cała aplikację w formie graficznej (okienkowej).Stanowi trzon aplikacji okienkowej
     **/
    public void guiMenu() throws IOException
    {
        setComponents();
        walutaListener();
        powierzchniaListener();
        wagaListener();
        languageListener();
        mouseListener();
    }


    //-----------------------------------------------------


    public void startGraficApp()
    {
        threadMainApp.setPriority(5);
        threadMainApp.start();

        threadIcon.setPriority(2);
        threadIcon.start();
    }


    //------------------------------------------------------


    Thread threadMainApp = new Thread(() -> {
        try
        {
            guiMenu();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    });


    Thread threadIcon = new Thread()
    {
        public void run() {
            grafic = new JLabel("");
            grafic.setBounds(10, 180, 65, 73);
            mainPanel.add(grafic);
            String[] nameIcon = {"Duke1.jpg", "Duke2.jpg", "Duke3.jpg", "Duke4.jpg", "Duke5.jpg", "Duke6.jpg"};

            int j = 0;

            while (true)
            {
                try {
                    j++;
                    for (int i = 0; i < 5; i++) {
                        grafic.setIcon(new javax.swing.ImageIcon(getClass().getResource(nameIcon[i])));
                        sleep(100);

                        synchronized (this){
                            while(suspended){
                                System.out.println(threadIcon.toString() + "SUSPEND");
                                this.wait();
                            }
                        }
                    }


                    for (int i = 4; i >= 0; i--) {
                        grafic.setIcon(new javax.swing.ImageIcon(getClass().getResource(nameIcon[i])));
                        sleep(100);

                        synchronized (this){
                            while(suspended){
                                System.out.println(threadIcon.toString() + "SUSPEND");
                                this.wait();
                            }
                        }
                    }


                    if (j == 10) {
                        grafic.setIcon(new javax.swing.ImageIcon(getClass().getResource(nameIcon[5])));
                        sleep(2000);
                        j = 0;
                    }

                    synchronized (this){
                        while(suspended){
                            System.out.println(threadIcon.toString() + "SUSPEND");
                            this.wait();
                        }
                    }

                }catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    void suspend(){
        suspended = true;
    }


    void resume(Thread thread) {
        suspended = false;
        synchronized(thread){
            thread.notify();
        }
    }




    private void dukeMouse(MouseEvent e) {
        String autor;
        String data;
        if (chooseLanguage == 1) {
            autor = "Author : ";
            data = "Date : ";
        } else{
            autor = "Autor : ";
            data = "Data : ";
         }

        String Creator = autor + " Michał Grulkowski" + "\n" + data + "10.04.2020";

        JOptionPane.showMessageDialog(null, Creator);

    }


    public void mouseListener() {
        grafic.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dukeMouse(e);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

    }

}


