/**
 * Konwerter podstawowych jednostek stosowanych w Anglii - Wersja 0.4 beta
 * Względem ostatniej wersji poprawiono:
 *
 * Zmieniono wygląd graficzny Gui poprzez:
 * 1) Dodanie animacji (Duke - maskotka Javy) w dolnym lewym rogu aplikacji.
 * 2) Dodanie JComboBox do zmiany języka.
 *
 * Dodano obsługę wątków (Thread):
 * 1) Jeden główny wątek na całą część programu, dotyczącej bezpośrednio działania konwersji, jak i samej aplikacji.
 * 2) Drugi wątek na potrzeby animacji wcześniej wspomnianej (Duke)

 * Zmieniono mechanikę działania gui poprzez:
 * 1) Możliwość zmiany języka aplikacji na angielski. Automatycznie poprzez określenie lokalizacji w systemie (Locale)
 *    lub ręcznie poprzez wybranie języka w prawym górnym rogu aplikacji.
 * 2) Po kliknięciu ikonkę Duke wyświetlają się informacje o autorze.
 *
 * Michał Grulkowski
 * Informatyka
 * Semestr 4
 * WEiI Koszlin
 */


/** Klasa Main to główna kalsa aplikacji uruchamiająca aplikację okienkową lub konsolową*/
public class Converter
{
    public static void main(String[] args)
    {
        GuiWindow graficApp = new GuiWindow();
        graficApp.startGraficApp();                           //Wersja okienkowa


        //ConsoleMenu textMenu = new ConsoleMenu();
        //textMenu.txtMenu();                           //Wersja konsolowa
    }
}
