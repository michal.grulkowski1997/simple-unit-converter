import java.util.Scanner;

class Weight
{

    public static double pound2kg(double pound)        //Metoda zamieniająca funty (waga)na kilogramy
    {
        return(pound*0.45359);
    }
    public static double ounce2kg(double ounce)        //Metoda zamieniająca uncje na kilogramy
    {
        return(ounce*0.028349);
    }

    public static void weight()
    {
        do
        {
            System.out.println("Wybrałeś - Waga");
            System.out.println("Jakiej konwersji chcesz dokonać?");
            System.out.println("1. Funty - na kilogramy");
            System.out.println("2. Uncje - na kilogramy");
            System.out.println("3. - Wyjście do menu");

            //Obsługa wyjątku podczas podania znaku zamiast oczekiwanej wartości liczbowej
            try
            {
                Scanner scan = new Scanner(System.in);
                int wybor = scan.nextInt();
                switch (wybor)
                {
                    case 1:
                    {
                        System.out.println("Podaj wartość w funtach : ");
                        double value = scan.nextDouble();
                        System.out.println("Wynik w kilogramach: " + pound2kg(value));
                    }break;
                    case 2:
                    {
                        System.out.println("Podaj wartość w uncjach : ");
                        double value = scan.nextDouble();
                        System.out.println("Wynik w kilogramach: " + ounce2kg(value));
                    }break;
                    case 3:
                    {
                        return;     //Wyjście z nieskończonej pętli do menu
                    }
                    default: System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji"); break;
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji");
            }

        }while (true);      //Pętla programowa nieskończona
    }
}