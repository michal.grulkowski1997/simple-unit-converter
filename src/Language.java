import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Klasa odpowiedzialna za pobranie wersji jezykowej aplikacji
 * */

public class Language
{
    /** Utworzenie pliku o nazwie LogKonwerter na którym będą przeprowadzane żądzne operacje */
    private File languageFile = new File("Language.txt");
    private String[][] languageVersion = new String[2][24];
    private int chooseLanguage;

    private String sendErrorMessage(){
        if (chooseLanguage == 0){
            return "Plik do odczytu wersji językowej nie istnieje aplikacja nie może zostać poprawine uruchomiona";
        }
        else{
            return "The file to read the language version does not exist, the application cannot be started correctly";
        }
    }

    public Language() {
        try {
            if (!languageFile.exists()) {
                JOptionPane.showMessageDialog(null, sendErrorMessage());
                System.exit(1);
            }

            String line;
            int indexLine_i = 0;
            int indexLine_j = 0;

            BufferedReader read = new BufferedReader(new FileReader("Language.txt"));

            while ((line = read.readLine()) != null) {
                languageVersion[indexLine_j][indexLine_i] = line;
                indexLine_i++;

                if (indexLine_i == 24) {
                    indexLine_i = 0;
                    indexLine_j++;
                }
            }
            read.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public String getLanguageVersion(int i, int j) {
        return languageVersion[i][j];
    }


    public void setChooseLanguage(int appLanguage) {
        chooseLanguage = appLanguage;

    }
}
