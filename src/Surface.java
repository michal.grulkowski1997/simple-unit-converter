import java.util.Scanner;

class Surface
{
    public static double sf2m_kw(double sqrft)
    {
        return(sqrft*0.0929);       //Metoda zamieniająca stopy na metry kwadratowe
    }

    public static double acres2m_kw(double acres)
    {
        return(acres*4046.85);      //Metoda zamieniająca akry na metry kwadratowe
    }

    public static void surface()
    {
        do
        {
            System.out.println("Wybrałeś - Powierzchnia");
            System.out.println("Jakiej konwersji chcesz dokonać?");
            System.out.println("1. - Stopy kwadratowe na metry kwadratowe");
            System.out.println("2. - Akry na metry kwadratowe");
            System.out.println("3. - Wyjście do menu");

            //Obsługa wyjątku podczas podania znaku zamiast oczekiwanej wartości liczbowej
            try
            {
                Scanner scan = new Scanner(System.in);
                int wybor = scan.nextInt();
                switch (wybor)
                {
                    case 1:
                    {
                        System.out.println("Podaj wartość w stopach: ");
                        double value = scan.nextDouble();
                        System.out.println("Wynik w metrach kwadratowych: " + sf2m_kw(value));
                    }break;
                    case 2:
                    {
                        System.out.println("Podaj wartość w akrach: ");
                        double value = scan.nextDouble();
                        System.out.println("Wynik w metrach kwadratowych: " + acres2m_kw(value));
                    }break;
                    case 3:
                    {
                        return;     //Wyjście z nieskończonej pętli do menu
                    }
                    default: System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji"); break;
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji");
            }

        }while (true);      //Pętla programowa nieskończona
    }
}