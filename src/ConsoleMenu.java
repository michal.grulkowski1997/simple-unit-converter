import java.util.Scanner;

/** Klasa ConsoleMenu zawiera fragmęt kodu do wywołania aplkikacji w wersji konsolowej. Na jej rzecz wywoływane są
 *  inne metody zawarte w klasach Currency, Surface oraz Weight
 **/
class ConsoleMenu
{
    /** Metoda publiczna stanowiąca trzon aplikacji konsolowej */
    public void txtMenu()
    {
        do
        {
            System.out.println("Menu:");
            System.out.println("Wybierz opcję przelicznika");
            System.out.println("1. - Walut");
            System.out.println("2. - Powierzchni");
            System.out.println("3. - Waga");
            System.out.println("4. - Wyjście");
            System.out.print("Twój wybór : ");

            //Obsługa wyjątku podczas podania znaku zamiast oczekiwanej wartości liczbowej
            try
            {
                Scanner in = new Scanner(System.in);
                int wybor = in.nextInt();
                System.out.println(wybor);

                switch (wybor)
                {
                    case 1: Currency.currency();    break;
                    case 2: Surface.surface();      break;
                    case 3: Weight.weight();        break;
                    case 4: exit_now();             break;
                    default: System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji");  break;
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji");
            }

        }while (true);      //Pętla programowa nieskończona
    }

    //Metoda wyjścia z programu
    private static void exit_now()
    {
        System.out.println("Dziękuję za skorzystanie z usługi");
        System.exit(0);
    }
}