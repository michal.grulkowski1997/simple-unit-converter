import java.util.Scanner;

class Currency
{
    //Metoda zamieniająca funty na pln
    public static double pound2PLN(double pound)
    {
        return(pound*5.03);
    }

    //Metoda zamieniająca euro na pln
    public static double euro2PLN(double euro)
    {
        return(euro*4.33);
    }

    public static void currency()
    {
        do
        {
            System.out.println("Wybrałeś - Waluta");
            System.out.println("Jakiej konwersji chcesz dokonać?");
            System.out.println("1. - Funt na PLN");
            System.out.println("2. - Euro na PLN");
            System.out.println("3. - Wyjście do menu");

            //Obsługa wyjątku podczas podania znaku zamiast oczekiwanej wartości liczbowej
            try
            {
                Scanner scan = new Scanner(System.in);
                int wybor = scan.nextInt();
                switch (wybor)
                {
                    case 1:
                    {
                        System.out.println("Podaj wartość w funtach: ");
                        double value = scan.nextDouble();
                        System.out.println("Wynik w PLN: " + pound2PLN(value));
                    }break;
                    case 2:
                    {
                        System.out.println("Podaj wartość w euro: ");
                        double value = scan.nextDouble();
                        System.out.println("Wynik w PLN: " + euro2PLN(value));
                    }break;
                    case 3:
                    {
                        return;     //Wyjście z nieskończonej pętli do menu
                    }
                    default: System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji"); break;
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Podaj odpowiednią cyfrę przy wybranej opcji");
            }

        }while (true);      //Pętla programowa nieskończona
    }
}